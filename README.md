# 🧠 JLBrainPhysio: A Julia Library for Brain Modeling

## 🚀 Introduction
JLBrainPhysio is designed as a Julia library for modeling brain physiology using 0D lumped models, based on the robust capabilities of the **[ModelingToolkit](https://docs.sciml.ai/ModelingToolkit.jl/stable/)** library. This library excels in scientific computing and machine learning, providing tools for symbolic-numeric computation which enhance and simplify model building.

## 🧩 Features of ModelingToolkit
ModelingToolkit offers a high-level description for differencial equations models that undergo symbolic preprocessing to improve efficiency and accuracy. It facilitates the automatic generation of functions such as Jocabians and applies transformations to simplify models, making numerical solving processes more straightforward. Explore more about ModelingToolkit on its [GitHub page](https://github.com/SciML/ModelingToolkit.jl).

## 🌐 JLBrainPhysio's Vision
JLBrainPhysio aims to modularize brain components like arteries, veins, and CSF compartments as blocks that can be assembled into an integrative model of brain physiology. This allow for flexibility in the construction of a model. We also included existing models of heart function and intrathoracic pressure to simulate complex interactions within the body that influence brain physiology.

## 📅 Future Directions
🌌 **Expanding the Universe of Brain Modeling:** JLBrainPhysio plans to include effects of gravity and physiological regulatory processes such as baroreflex and autoregulation mechanisms.

## 🛠️ Get Involved
Join us in enhancing JLBrainPhysio by exploring its structure and contributing to its development through the ModelingToolkit ecosystem. Your expertise can help in pushing the boundaries of neurofluid dynamics and pathophysiology research.


## Installation

To use JLBrainPhysio, you must have Julia installed on your system. If you haven't installed Julia yet, download and install it from [the official Julia website](https://julialang.org/downloads/).

### Setting Up the Project

1. **Clone the Repository**

   First, clone this repository to your local machine using:

   ```
   git clone https://github.com/AlexandraVallet/JLBrainPhysio.git
   ```

2. **Activate and Instantiate the Project**

   Navigate to the project directory:

   ```
   cd JLBrainPhysio
   ```

   Open Julia and activate the project environment:

   ```julia
   using Pkg
   Pkg.activate(".")
   Pkg.instantiate()
   ```

   This will install all necessary dependencies as specified in `Project.toml`.

## Usage

### Running Notebooks

To use the modules in Jupyter or Pluto notebooks:

1. Ensure you have either Jupyter or Pluto installed. If not, you can add them via Julia's package manager:

   ```julia
   using Pkg
   Pkg.add("IJulia") # For Jupyter
   # or
   Pkg.add("Pluto") # For Pluto
   ```

2. Navigate to the `notebooks` directory and start your notebook server. For Jupyter:

   ```
   jupyter notebook
   ```

   For Pluto, start Julia and then:

   ```julia
   using Pluto
   Pluto.run()
   ```

   Make sure to activate the project environment as described in the Installation section if your notebook is in a different directory.

### Using the Module

In your Julia scripts or notebooks, you can load and use the module as follows:

```julia
using Pkg
Pkg.activate(".") # Adjust the path as necessary
using JLBrainPhysio

# Example usage
JLBrainPhysio.someFunction()
```

Replace `JLBrainPhysio` and `someFunction` with your actual project and function names.

## Contributing

Contributions to JLBrainPhysio are welcome! Please refer to CONTRIBUTING.md for guidelines on how to make contributions.

## License

JLBrainPhysio is released under the [MIT License](LICENSE). See the LICENSE file for more details.