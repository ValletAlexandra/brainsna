"""
`ConstantPressure(;name, P=1.0)`

Implements a constant pressure source to a system.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`P`:     Constant pressure in mmHg
"""

@component function ConstantPressure(; name, P=0.0)
        @named node = Pin()
        sts = []
        ps = @parameters P = P   
        eqs = [
                node.p ~ P 
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), node)
end


"""
`ConstantFlow(;name, Q=1.0)`

Implements a constant flow source to a system.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`Q`:     Constant flow in cm^3/s (ml/s)
"""

@component function ConstantFlow(; name, Q=0.0)
        @named node = Pin()
        sts = []
        ps = @parameters Q = Q   
        eqs = [
                node.q ~ Q
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), node)
end

"""
`DrivenPressure(;name, P=1.0, fun)`

Implements a driven pressure source to a system modulated by a function provided.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`P`:     Constant pressure in mmHg

`fun`:   Function which modulates the input
"""
@component function DrivenPressure(; name, Factor=1.0, fun)
        @named node = Pin()
        sts = []
        ps = @parameters Factor = Factor   
        eqs = [
                node.p ~ Factor * fun(t)
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), node)
end


"""
`DrivenFlow(;name, Q=1.0, fun)`

Implements a driven flow source to a system.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`Q`:     Constant flow in cm^3/s (ml/s).

`τ`     Length of cardiac cycle is s

`fun`:   Function which modulates the input
"""
@component function DrivenFlow(; name, Factor=1.0, fun)
        @named node = Pin()
        sts = []
        ps = @parameters Factor = Factor   
        eqs = [
                node.q ~ Factor * fun(t)
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), node)
end

"""
`WK2BC(;name, R=1.0, C=1.0, P=0.0)`

Implements the 2 element windkessel model connected to a ground pin.

Parameters are in the cm, g, s system.
Pressure in mmHg.
Volume in ml.
Flow in cm^3/s (ml/s)

Named parameters:

`R`:      Resistance in mmHg*s/ml

`C`:      Compliance in ml/mmHg

`P`:      Fixed pressure in mmHg
"""
@component function WK2BC(; name, R=1.0, C=1.0, P=0.0)
        @named node = Pin()
    
        sts = @variables Δp(t) = 0.0 q(t) = 0.0
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []

        # These are the components the subsystem is made of:
        @named R = Resistor(R=R)
        @named C = Capacitor(C=C)
        @named ground = Ground(P=P)
    
        eqs = [
                Δp ~ ground.g.p - node.p
                q ~ node.q
                connect(node, R.in, C.in)
                connect(R.out, C.out, ground.g)
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), node, R, C, ground)
end
