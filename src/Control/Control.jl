@component function CerebralFlowRegulation(; name, τ=2, P_an=100, P_vn=20)
    @named pin_art = Pin()
    @named pin_ven = Pin()


    sts = @variables begin
        x(t) = 0
    end

    # No parameters in this function
    # Parameters are inherited from subcomponents
    ps = @parameters τ = τ P_an= P_an P_vn = P_vn


    D = Differential(t)

    eqs = [
            D(x) ~ -1/τ *x+1/ τ*((pin_art.p-pin_ven.p-P_an+P_vn)/(P_an-P_vn)),
            pin_art.q ~ 0,
            pin_ven.q ~ 0
    ]

    # and finaly compose the system
    compose(ODESystem(eqs, t, sts, ps; name=name), pin_art, pin_ven)
end