@component function Ground(; name, P=0.0)
    @named g = Pin()
    ps = @parameters P = P
    eqs = [g.p ~ P]
    compose(ODESystem(eqs, t, [], ps; name=name), g)
end

"""
`ForcedFlow(;name, Q=1.0)`

Implements a constant forced flow for active transfer.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`Q`:     Constant flow in cm^3/s (ml/s)
"""
@component function ForcedFlow(; name, Q=2.0)
        @named in = Pin()
        @named out = Pin()
        sts = []
        eqs = [
                out.q ~ - Q 
                in.q ~ Q
        ]
        compose(ODESystem(eqs, t, sts, []; name=name), in, out)
end
    

"""
`Resistor(;name, R=1.0)`

Implements the resistor using Ohm's law to represent a vessels linear resistance to blood flow.

Parameter is in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg
`q` calculated in cm^3/s (ml/s)

Named parameters:

`R`:       Resistance of the vessel to the fluid in mmHg*s/ml
"""
@component function Resistor(; name, R=1.0)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters R = R
        eqs = [
                Δp ~ -q * R
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end

"""
`ResistorDiode(;name, R=1e-3)`

Implements the resistance across a valve following Ohm's law exhibiting diode like behaviour.

Parameters are in the cm, g, s system.
Pressure in mmHg.
Flow in cm^3/s (ml/s)

Named parameters:

`R`     Resistance across the valve in mmHg*s/ml
"""
@component function ResistorValve(; name, R=1e-3)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters R = R
        eqs = [
                q ~ -Δp / R * (Δp < 0)
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end





"""
`QResistor(;name, K=1.0)`

Implements the quadratic resistor to represent a vessels non-linear resistance to blood flow.

Parameters are in the cm, g, s system.
Pressures in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`K`: non-linear resistance of the vessel to the fluid in mmHg*s^2/ml^2
"""
@component function QResistor(; name, K=1.0)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters K = K
        eqs = [
                Δp ~ -q * abs(q) * K
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end


"""
`Capacitor(;name, C=1.0)`

Implements a capacitor to represent vessel capacitance.

Parameters are in the cm, g, s system.
Pressures in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`C`:      capacitance of the vessel in ml/mmHg
"""
@component function Capacitor(; name, C=1.0)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters C = C
        D = Differential(t)
        eqs = [
                D(Δp) ~ -q / C
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end



"""
`Inductance(;name, L=1.0)`

Implements the inductance to represent blood inertance.

Parameters are in the cm, g, s system.
Pressures in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`L`:       Inertia of the fluid in mmHg*s^2/ml
"""
@component function Inductance(; name, L=1.0)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters L = L
        D = Differential(t)
        eqs = [
                D(q) ~ -Δp / L
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end



"""
`Compliance(; name, V₀=0.0, C=1.0, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)`

Implements the compliance of a vessel : TODO need to simplify here, this is the original model from CirculatorySystemModels

Parameters are in the cm, g, s system.
Pressure in mmHg.
`p` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`V₀`:               Unstressed volume ml

`C`:                Vessel compliance in ml/mmHg


`inP`:             (Bool) formulate in dp/dt (default: false)

`has_ep`:          (Bool) if true, add a parameter `p₀` for pressure offset
                   e.g., for thoracic pressure (default: false)

`p₀`:              External pressure in mmHg (e.g., thorax pressure, default: 0.0)
                   _Note: if this argument is set, it will be used, even if `has_ep` is
                   `false`. `has_ep` only controls if `p₀` will be exposed as a parameter!_

has_variable_ep`: (Bool) expose pin for variable external pressure (default: false)
                   This pin can be connected to another pin or function providing external pressure.
                   _Note: if `has_variable_ep` is set to `true` this pin is created, independent of
                   `has_ep`!_
"""
@component function Compliance(; name, V₀=0.0, C=1.0, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)
        @named in = Pin()
        @named out = Pin()

        if has_variable_ep
                @named ep = Pin()
        end

        sts = @variables begin
                V(t) = V₀
                p(t) = 0.0
        end

        ps = @parameters begin
                V₀ = V₀
                C = C
        end

        # Add the thoracic pressure variant

        D = Differential(t)

        eqs = [
                0 ~ in.p - out.p
                p ~ in.p
        ]

        if has_variable_ep
                push!(sts,
                        (@variables p_rel(t) = p₀)[1]
                )
                if has_ep
                        push!(ps,
                                (@parameters p₀ = p₀)[1]
                        )
                end
                push!(eqs,
                        p_rel ~ ep.p + p₀,
                        ep.q ~ 0
                )
        elseif has_ep
                push!(ps,
                        (@parameters p₀ = p₀)[1]
                )
                p_rel = p₀
        else
                p_rel = p₀
        end

        if inP
                push!(eqs,
                        V ~ (p - p_rel) * C + V₀,
                        D(p) ~ (in.q + out.q) * 1 / C
                )
        else
                push!(eqs,
                        p ~ (V - V₀) / C + p_rel,
                        D(V) ~ in.q + out.q
                )
        end

        if has_variable_ep
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out, ep)
        else
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out)
        end
end




"""
`Compliance(; name, V₀=0.0, C₀=1.0, inP=false)

Linear compliance of a vessel, depending on the transmural pressure

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).


Named parameters:

`V₀`:              Unstressed volume ml

`C₀`:              Unstressed compliance in ml/mmHg

`inP`:            (Bool) formulate in dp/dt (default: false)


"""
#Todo : for now I use constant compliance parameters.But it should be proportionnal to pressure. To be tested when in physiological conditions and initialisation
@component function Linear_Compliance(; name, C₀=1.0, V₀=1.0)
        @named in = Pin()
        @named out = Pin()
    
        ps = @parameters begin
            C₀ = C₀
            V₀ = V₀
        end
     
        sts = @variables begin
            V(t) = V₀
            p_ext(t) = 0.0
            p(t) = 0.0
        end
    
    
        D = Differential(t)
    
        eqs = [
                p ~ in.p
                p_ext ~ out.p
                out.q ~ -in.q
                D(V) ~ in.q
                in.q ~  (D(p)-D(p_ext))* C₀  
        ]
    
    
    
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out)
        
end 

@component function NonLinear_Compliance(; name, C₀=1.0, V₀=1.0)
        @named in = Pin()
        @named out = Pin()
    
        ps = @parameters begin
            C₀ = C₀
            V₀ = V₀
        end
     
        sts = @variables begin
            V(t) = V₀
        end
    
    
        D = Differential(t)
    
        eqs = [
                D(V) ~ in.q
                in.q * (in.p-out.p) ~  (D(in.p)-D(out.p))* C₀
        ]
    
    
    
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out)
        
end 