"""
`WK2(;name, Rc=1.0, C=1.0)`

Implements the 2 element windkessel model.

Parameters are in the cm, g, s system.
Pressure in mmHg.
Volume in ml.
Flow in cm^3/s (ml/s)

Named parameters:

`Rc`:      Resistance in mmHg*s/ml

`C`:       Compliance in ml/mmHg
"""
@component function WK2(; name, R=1.0, C=1.0)
        @named in = Pin()
        @named out = Pin()

        sts = @variables Δp(t) = 0.0 q(t) = 0.0
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []

        # These are the components the subsystem is made of:
        @named R = Resistor(R=R)
        @named C = Capacitor(C=C)

        eqs = [
                Δp ~ out.p - in.p
                0 ~ in.q + out.q
                q ~ in.q
                connect(in, R.in, C.in)
                connect(R.out, C.out, out)
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out, R, C)
end




"""
`WK3(;name, Rc=1.0, Rp=1.0, C=1.0)`

Implements the 3 element windkessel model.

Parameters are in the cm, g, s system.
Pressure in mmHg.
Volume in ml.
Flow in cm^3/s (ml/s)

Named parameters:

`Rc`:      Characteristic impedence in mmHg*s/ml

`Rp`:      Peripheral resistance in mmHg*s/ml

`C`:       Arterial compliance in ml/mmHg
"""
@component function WK3(; name, Rc=1.0, Rp=1.0, C=1.0)
        @named in = Pin()
        @named out = Pin()

        sts = @variables Δp(t) = 0.0 q(t) = 0.0
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []

        # These are the components the subsystem is made of:
        @named Rc = Resistor(R=Rc)
        @named Rp = Resistor(R=Rp)
        @named C = Capacitor(C=C)
        @named ground = Ground()

        eqs = [
                Δp ~ out.p - in.p
                0 ~ in.q + out.q
                q ~ in.q
                connect(in, Rc.in)
                connect(Rc.out, Rp.in, C.in)
                connect(Rp.out, C.out, out)
        ]

        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out, Rc, Rp, C)
end