# Cycle time in seconds

τ = 0.85

# Double Hill parameters for the ventricle

Eₘᵢₙ = 0.03
Eₘₐₓ = 1.5
n1LV = 1.32
n2LV = 21.9
Tau1fLV = 0.303 * τ
Tau2fLV = 0.508 * τ


# Valve parameters
# Aortic valve basic
Zao = 0.033
# Mitral valve basic
Rmv = 0.006

# Inital Pressure (mean cardiac filling pressure)
MCFP = 7.0

# ## Calculating the additional `k` parameter
#
# The ventricle elastance is modelled as:
#
# $$E_{l v}(t)=\left(E_{\max }-E_{\min }\right) e(t)+E_{\min }$$
#
# where $e$ is a double-Hill function, i.e., two Hill-functions, which are multiplied by each other:
#
# $$e(\tau)= k \times \frac{\left(\tau / \tau_1\right)^{n_1}}{1+\left(\tau / \tau_1\right)^{n_1}} \times \frac{1}{1+\left(\tau / \tau_2\right)^{n_2}}$$
#
# $k$ is a scaling factor to assure that $e(t)$ has a maximum of $e(t)_{max} = 1$:
#
# $$k = \max \left(\frac{\left(\tau / \tau_1\right)^{n_1}}{1+\left(\tau / \tau_1\right)^{n_1}} \times \frac{1}{1+\left(\tau / \tau_2\right)^{n_2}} \right)^{-1}$$ .
#

nstep = 1000
t = LinRange(0, τ, nstep)

kLV = 1 / maximum((t ./ Tau1fLV) .^ n1LV ./ (1 .+ (t ./ Tau1fLV) .^ n1LV) .* 1 ./ (1 .+ (t ./ Tau2fLV) .^ n2LV))


##########3
@parameters begin
    # Cycle time in seconds
    τ = 0.85
    # Double Hill parameters for the ventricle
    Eₘᵢₙ = 0.03
    Eₘₐₓ = 1.5
    n1LV = 1.32
    n2LV = 21.9
    Tau1fLV = 0.303 * τ
    Tau2fLV = 0.508 * τ
    # Valve parameters
    # Aortic valve basic
    Zao = 0.033
    # Mitral valve basic
    Rmv = 0.006
    # Inital Pressure (mean cardiac filling pressure)
    MCFP = 7.0
    nstep = 1000
    t = LinRange(0, τ, nstep)
    kLV = 1 / maximum((t ./ Tau1fLV).^n1LV ./ (1 .+ (t ./ Tau1fLV).^n1LV) .* 1 ./ (1 .+ (t ./ Tau2fLV).^n2LV))
end

@variables begin
    Δp(t) = 0.0
    q(t) = 0.0
end
@components begin
    in = Pin()
    out = Pin()
    LV = DHChamber(V₀=0.0, Eₘₐₓ=Eₘₐₓ, Eₘᵢₙ=Eₘᵢₙ, n₁=n1LV, n₂=n2LV, τ=τ, τ₁=Tau1fLV, τ₂=Tau2fLV, k=kLV, Eshift=0.0, inP=true)
    AV = ResistorDiode(R=Zao)
    MV = ResistorDiode(R=Rmv)
end

@equations begin
    Δp ~ out.p - in.p
    q ~ in.q
    connect(LV.out, AV.in)
    connect(AV.out, out)
    connect(in, MV.in)
    connect(MV.out, LV.in)
end