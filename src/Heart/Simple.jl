"""
`DHelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)`

Helper function for `DHChamber`
"""
function DHelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)
        tᵢ = rem(t + (1 - Eshift) * τ, τ)

        return (Eₘₐₓ - Eₘᵢₙ) * k * ((tᵢ / τ₁)^n₁ / (1 + (tᵢ / τ₁)^n₁)) * (1 / (1 + (tᵢ / τ₂)^n₂)) + Eₘᵢₙ
end


"""
`DHdelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)`

Helper function for `DHChamber`
"""
function DHdelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)
        tᵢ = rem(t + (1 - Eshift) * τ, τ)

        de = ((Eₘₐₓ - Eₘᵢₙ) * k * (τ₂^n₂ * n₁ * tᵢ^(n₁ - 1) *
                                   (τ₁^n₁ * τ₂^n₂ + τ₁^n₁ * tᵢ^n₂ + τ₂^n₂ * tᵢ^n₁ + tᵢ^(n₁ + n₂)) -
                                   τ₂^n₂ * tᵢ^n₁ * (τ₁^n₁ * n₂ * tᵢ^(n₂ - 1) + τ₂^n₂ * n₁ * tᵢ^(n₁ - 1) +
                                                    (n₁ + n₂) * tᵢ^(n₁ + n₂ - 1))) /
              (τ₁^n₁ * τ₂^n₂ + τ₁^n₁ * tᵢ^n₂ + τ₂^n₂ * tᵢ^n₁ + tᵢ^(n₁ + n₂))^2)

        return de
end

"""
`DHChamber(;name, V₀, Eₘᵢₙ, n₁, n₂, τ, τ₁, τ₂, k, Eshift=0.0, inP=false)`

The Double Hill chamber/ventricle model is defined based on the vessel
element, but has a time varying elastance function modelling the contraction
of muscle fibres

The time varying elastance is calculated using the Double Hill model.

This model uses external helper functions `elastance` and `delastance`
which describe the elastance function and the first derivative of it.

It calculates the elastance as:

E(t) = (Eₘₐₓ - Eₘᵢₙ) * e(t) + Eₘᵢₙ

where e(t) is the Double-Hill function.

Named parameters:

`V₀`:     stress-free volume (zero pressure volume)

`p₀`      pressure offset (defaults to zero)
          this is present in some papers (e.g. Shi), so is
          provided here for conformity. Defaults to 0.0

`Eₘᵢₙ`:   minimum elastance

`Eₘₐₓ`:   maximum elastance

`n₁`:     rise coefficient

`n₂`:     fall coefficient

`τ`:      pulse length [s]

`τ₁`:     rise timing parameter[s]

`τ₂`:     fall timimg paramter [s]

`k`:      elastance factor*

`Eshift`: time shift of contraction (for atria)

`inP`:    (Bool) formulate in dp/dt (default: false)

*Note: `k` is not an independent parameter, it is a scaling factor that corresponds
to 1/max(e(t)), which ensures that e(t) varies between zero and 1.0, such that
E(t) varies between Eₘᵢₙ and Eₘₐₓ.
"""
@mtkmodel DHChamber begin
        @components begin
                in = Pin()
                out = Pin()
        end
        @structural_parameters begin
                inP = false
        end
        @variables begin
                V(t) = 2.0
                p(t) = 0.0
        end
        @parameters begin
                V₀
                p₀ = 0.0
                Eₘᵢₙ
                Eₘₐₓ
                n₁
                n₂
                τ
                τ₁
                τ₂
                k
                Eshift = 0.0
        end

        begin
                E = DHelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)
                DE = DHdelastance(t, Eₘᵢₙ, Eₘₐₓ, n₁, n₂, τ, τ₁, τ₂, Eshift, k)
                p_rel = p₀
        end

        @equations begin
                0 ~ in.p - out.p
                p ~ in.p
                if inP
                        V ~ (p - p_rel) / E + V₀
                        D(p) ~ (in.q + out.q) * E + (p - p_rel) / E * DE
                else
                        p ~ (V - V₀) * E + p_rel
                        D(V) ~ in.q + out.q
                end
        end
end
