module JLBrainPhysio

using ModelingToolkit, DifferentialEquations

@variables t
D = Differential(t)

export smooth_cos
include("Maths.jl")

export Pin, OnePort
include("Utils.jl")

export  Ground, Resistor, QResistor, ResistorValve, Capacitor, Inductance, ForcedFlow, Compliance, Linear_Compliance, NonLinear_Compliance
include("Elements/Elements.jl")


export  NonLinear_Vessel,Linear_Vessel,Exchange_Vessel
include("Vessels/Vessels.jl")


export  ConstantPressure, ConstantFlow, DrivenPressure, DrivenFlow , WK2BC
include("BoundaryConditions/BoundaryConditions.jl")


export DHChamber#, ShiChamber, ShiAtrium, ShiHeart, WK3, WK3E, CR, CRL, RRCR, ShiSystemicLoop, ShiPulmonaryLoop, ResistorDiode, OrificeValve, ShiValve, MynardValve_SemiLunar, MynardValve_Atrioventricular
include("Heart/Simple.jl")



end # module