# Define and register smooth functions
# These are "smooth" aka differentiable and avoid Gibbs effect
# These follow: `offset` + `smooth_wave` * `smooth_step` with zero output for `t < start_time`
function smooth_cos(x, δ, f, amplitude, ϕ, offset, start_time)
    offset +
    amplitude * cos(2 * π * f * (x - start_time) + ϕ) *
    smooth_step(x, δ, one(x), zero(x), start_time)
end

function smooth_step(x, δ, height, offset, start_time)
    offset + height * (atan((x - start_time) / δ) / π + 0.5)
end

# more signal math functions : https://github.com/SciML/ModelingToolkitStandardLibrary.jl/blob/main/src/Blocks/math.jl
# https://github.com/SciML/ModelingToolkitStandardLibrary.jl/blob/main/src/Blocks/sources.jl
