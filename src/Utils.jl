"""
    Pin(; name)

A pin in an analog circuit.

# States:
- `v(t)`: [`V`] The voltage at this pin
- `i(t)`: [`A`] The current passing through this pin
""" 
@connector function Pin(; name)
    sts = @variables p(t) = 1.0 q(t) = 1.0 [connect = Flow]
    ODESystem(Equation[], t, sts, []; name=name)
end


"""
    OnePort(; name, v = 0.0, i = 0.0)

Component with two electrical pins `p` and `n` and current `i` flows from `p` to `n`.

# States:

  - `v(t)`: [`V`] The voltage across component `p.v - n.v`
  - `i(t)`: [`A`] The current passing through positive pin

# Connectors:

  - `p` Positive pin
  - `n` Negative pin
"""
@component function OnePort(; name)
    @named in = Pin()
    @named out = Pin()
    sts = @variables Δp(t) = 0.0 q(t) = 0.0
    eqs = [
            Δp ~ out.p - in.p
            0 ~ in.q + out.q
            q ~ in.q
    ]
    compose(ODESystem(eqs, t, sts, []; name=name), in, out)
end