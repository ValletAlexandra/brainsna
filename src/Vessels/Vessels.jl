"""
`PoiseuilleResistor(;name, μ=3e-2, r=0.5, L=5)`

Implements the resistance following the Poiseuille law.

Parameters are in the cm, g, s system.
Pressures in mmHg.constant
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`μ`:       viscosity of fluid in dyne s / cm^2

`r`:       radius of vessel segmenty in cm

`L`:       length of vessel segment in cm
"""
@component function PoiseuilleResistor(; name, μ=3e-2, r=0.1, L=1)
        @named oneport = OnePort()
        @unpack Δp, q = oneport
        ps = @parameters μ = μ r = r L = L

        # Poiseille resistance in CGS units
        R = 8 * μ * L / (π * r^4) * (1 / 1333.2)
        # converted into physiological units.

        eqs = [
                Δp ~ -q * R
        ]
        extend(ODESystem(eqs, t, [], ps; name=name), oneport)
end

  


@component function Linear_Vessel(; name, R=1.0, C₀=1.0, V₀=1.0)
        @named in = Pin()
        @named out = Pin()
        @named ext = Pin()
    
        sts =  [] 
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []
    
        # These are the components the subsystem is made of:
        @named Rc= Resistor(R=R/2)
        @named Rp= Resistor(R=R/2)
        @named Ca = Linear_Compliance(C₀=C₀,V₀=V₀)
    
        eqs = [
                connect(in, Rc.in)
                connect(Rc.out, Rp.in, Ca.in)
                connect(Ca.out, ext)
                connect(Rp.out, out)
        ]
    
        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out,ext,Rc,Rp,Ca)
end

@component function NonLinear_Vessel(; name, R=1.0, C₀=1.0, V₀=1.0)
        @named in = Pin()
        @named out = Pin()
        @named ext = Pin()
    
        sts =  [] 
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []
    
        # These are the components the subsystem is made of:
        @named Rc= Resistor(R=R)
        @named Rp= Resistor(R=R)
        @named Ca = NonLinear_Compliance(C₀=C₀,V₀=V₀)
    
        eqs = [
                connect(in, Rc.in)
                connect(Rc.out, Rp.in, Ca.in)
                connect(Ca.out, ext)
                connect(Rp.out, out)
        ]
    
        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out,ext,Rc,Rp,Ca)
end


@component function Exchange_Vessel(; name, R=1.0, Re=1.0, Qe=1.0)
        @named in = Pin()
        @named out = Pin()
        @named ext = Pin()
    
        sts = []
        # No parameters in this function
        # Parameters are inherited from subcomponents
        ps = []
    
        # These are the components the subsystem is made of:
        @named Rc= Resistor(R=R/2)
        @named Rp= Resistor(R=R/2)
        @named active_drainage=ForcedFlow(Q=Qe)
        @named passive_drainage=Resistor(R=Re)
        
    
        eqs = [
                connect(in, Rc.in)
                connect(Rc.out, Rp.in,active_drainage.in,passive_drainage.in)
                connect(active_drainage.out, ext)
                connect(passive_drainage.out, ext)
                connect(Rp.out, out)
        ]
    
        # and finaly compose the system
        compose(ODESystem(eqs, t, sts, ps; name=name), in, out,ext,Rc,Rp,active_drainage,passive_drainage)
end


"""
`Elastance(; name, V₀=0.0, E=1.0, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)`

Implements the elastance of a vessel. Elastance more commonly used to describe the heart.

Parameters are in the cm, g, s system.
Pressure in mmHg.
`Δp` is calculated in mmHg,
`q` is calculated in cm^3/s (ml/s).

Named parameters:

`V₀`:             Unstressed volume ml

`E`:              Vessel elastance in ml/mmHg. Equivalent to compliance as E=1/C

`inP`:            (Bool) formulate in dp/dt (default: false)

`has_ep`:         (Bool) if true, add a parameter `p₀` for pressure offset
                  e.g., for thoracic pressure (default: false)

`p₀`:             External pressure in mmHg (e.g., thorax pressure, default: 0.0)
                  _Note: if this argument is set, it will be used, even if `has_ep` is
                  `false`. `has_ep` only controls if `p₀` will be exposed as a parameter!_

has_variable_ep`: (Bool) expose pin for variable external pressure (default: false)
                   This pin can be connected to another pin or function providing external pressure.
                   _Note: if `has_variable_ep` is set to `true` this pin is created, independent of
                   `has_ep`!_
"""
@component function Elastance(; name, V₀=0.0, E=1.0, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)
        @named in = Pin()
        @named out = Pin()

        if has_variable_ep
                @named ep = Pin()
        end

        sts = @variables begin
                V(t) = V₀
                p(t) = 0.0
        end

        ps = @parameters begin
                V₀ = V₀
                E = E
        end

        D = Differential(t)

        eqs = [
                0 ~ in.p - out.p
                p ~ in.p
        ]

        if has_variable_ep
            push!(sts,
                  (@variables p_rel(t) = p₀)[1]
                )
            if has_ep
                push!(ps,
                        (@parameters p₀ = p₀)[1]
                )
            end
            push!(eqs,
                  p_rel ~ ep.p + p₀,
                  ep.q ~ 0
                )
        elseif has_ep
                push!(ps,
                        (@parameters p₀ = p₀)[1]
                )
                p_rel = p₀
        else
                p_rel = p₀
        end

        if inP
                push!(eqs,
                        V ~ (p - p_rel) / E + V₀,
                        D(p) ~ (in.q + out.q) * E
                )
        else
                push!(eqs,
                        p ~ (V - V₀) * E + p_rel,
                        D(V) ~ in.q + out.q
                )
        end

        if has_variable_ep
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out, ep)
        else
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out)
        end
end


"""
`VariableElastance(; name, V₀=0.0, C=1.0, Escale=1.0, fun, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)`

`VariableElastance` is defined based on the `Elastance` element,
but has a time varying elastance function modelling
the contraction of muscle fibres.

Named parameters:

`V₀`:              stress-free volume (zero pressure volume)

`Escale`:          scaling factor (elastance factor)

`fun`:             function object for elastance (must be `fun(t)`)

`inP`:             (Bool) formulate in dp/dt (default: false)

`has_ep`:          (Bool) if true, add a parameter `p₀` for pressure offset
                   e.g., for thoracic pressure (default: false)

`p₀`:              External pressure in mmHg (e.g., thorax pressure, default: 0.0)
                   _Note: if this argument is set, it will be used, even if `has_ep` is
                   `false`. `has_ep` only controls if `p₀` will be exposed as a parameter!_

has_variable_ep`: (Bool) expose pin for variable external pressure (default: false)
                   This pin can be connected to another pin or function providing external pressure.
                   _Note: if `has_variable_ep` is set to `true` this pin is created, independent of
                   `has_ep`!_
"""
@component function VariableElastance(; name, V₀=0.0, C=1.0, Escale=1.0, fun, inP=false, has_ep=false, has_variable_ep=false, p₀=0.0)
        @named in = Pin()
        @named out = Pin()

        if has_variable_ep
                @named ep = Pin()
        end

        sts = @variables begin
                V(t) = V₀
                p(t) = 0.0
        end

        ps = @parameters begin
                V₀ = V₀
                C = C
        end

        D = Differential(t)
        E = Escale * fun(t)

        eqs = [
                0 ~ in.p - out.p
                p ~ in.p
        ]

        if has_variable_ep
                push!(sts,
                        (@variables p_rel(t) = p₀)[1]
                )
                push!(eqs,
                        p_rel ~ ep.p,
                        ep.q ~ 0
                )
        elseif has_ep
                push!(ps,
                        (@parameters p₀ = p₀)[1]
                )
                p_rel = p₀
        else
                p_rel = p₀
        end

        if inP
                push!(eqs,
                        V ~ (p - p_rel) / E + V₀,
                        D(p) ~ (in.q + out.q) * E + V * D(E(t))
                )
        else
                push!(eqs,
                        p ~ (V - V₀) * E + p_rel,
                        D(V) ~ in.q + out.q
                )
        end

        if has_variable_ep
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out, ep)
        else
                compose(ODESystem(eqs, t, sts, ps; name=name), in, out)
        end
end


